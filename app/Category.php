<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    // Deshabilitamos la asiganacion masiva
    protected  $guarded = [];

    // cada categoria tiene muchos post
    public function posts() {
        return $this->hasMany(Post::class);
    }


    public function setNameAttribute($name)
    {
        $this->attributes['name'] = $name;
        $this->attributes['slug'] = str_slug($name);
    }


    // Slug para redirigit al categoria
    public function getRouteKeyName() {
        return 'slug';
    }


}
