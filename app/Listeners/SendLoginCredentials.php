<?php

namespace App\Listeners;

use App\Events\UserWasCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Support\Facades\Mail;
use App\Mail\LoginCredentials;

class SendLoginCredentials
{

    public function handle(UserWasCreated $event)
    {
        // Enviar el email con las credenciales del login
        // to : destinatario sera al mismo usuario
        // queue : metodo de envio
        //  new LoginCredentials($event->user, $event->password) => y mandarle una nueva instacia
        Mail::to($event->user)->queue(
            new LoginCredentials($event->user, $event->password)
        );

    }
}
