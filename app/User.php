<?php

namespace App;

use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * App\User
 *
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @mixin \Eloquent
 */
class User extends Authenticatable
{
    use Notifiable,HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // Para devolver el password encriptado
    // y en registerController
    // 'password' => Hash::make($data['password']),
    // deshabilitar el Hash::make para evitar doble encriptacion
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }



    // cada usuario tiene muchos post
    public function posts() {
        return $this->hasMany(Post::class);
    }

    public function scopeAllowed($query)
    {
        // si es que el usuario tiene permiso del metodo view puede ver todos los usuarios
        // $THIS => le enviamos el usuario
       /* public function view(User $authUser, User $user)
        {
            // $authUser =   usuario autenticado
            // $user     =   el id del usuario
            return $authUser->id === $user->id || $user->hasPermissionTo('View users');
        }*/

       // Si es que no eres un administrador solo podras ver a ti mismo
        if( auth()->user()->can('view', $this) )
        {
            return $query;
        }

        // caso contrario solo puede puede ver solo
        return $query->where('id', auth()->id());
    }


    public function getRoleDisplayNames()
    {
        return $this->roles->pluck('display_name')->implode(', ');
    }


}
