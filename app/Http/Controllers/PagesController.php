<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;



class PagesController extends Controller
{

    public function __construct()
    {
        Carbon::setLocale('es');
    }

    public function spa()
    {
        return view('pages.spa');
    }



    public function index()
    {

       $query = Post::withCount(['photos'])->published();

        if(request('month')) {
            $query->where('published_at', 'LIKE', '%'.request('month').'%');
        }

        if(request('year')) {
            $query->where('published_at', 'LIKE', '%'.request('year').'%');
        }

       $posts = $query->paginate(10);


        // si laravel requiere de una respuesta json y tambien si es un llamado de ajax
        // retornamos todoslos posts
        if ( request()->wantsJson() )
        {
            return $posts;
        }



        return view('pages.home',compact('posts'));
    }

    public function about()
    {
        return view('pages.about');
    }

    public function archive()
    {

        // Buscare un un todos los post que sean publicos y que busque  byYearAndMonth
        $data =   [
            'authors' => User::latest()->take(2)->get(),
            'categories' => Category::take(7)->get(),
            // solo quiero mostrar 5 posts
            'posts' => Post::latest('published_at')->take(5)->get(),
            'archive' =>Post::published()->byYearAndMonth()->take(5)->get(),
        ];

        if (request()->wantsJson())
        {
            return $data;
        }

        return view('pages.archive', $data);


        return view('pages.archive',$data);




    }

    public function contact()
    {
        return view('pages.contact');
    }


}
