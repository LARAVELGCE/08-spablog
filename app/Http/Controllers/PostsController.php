<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class PostsController extends Controller
{
    public function show(Post $post)
    {
      // si es que cumple las condiciones para que sea publica
      // o esta autenticado
      // puede ver el detalle del post

        if ($post->isPublished() || auth()->check()) {

            // lo cargamos con sus relaciones
            $post->load('owner', 'category', 'tags', 'photos');
            // si el request necesita json o
            // si la peticion es ajax
            if (request()->wantsJson())
            {
                return $post;
            }


            return view('posts.show', compact('post' ));
        }

        abort(404);


    }



}
