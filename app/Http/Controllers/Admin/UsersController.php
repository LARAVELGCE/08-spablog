<?php

namespace App\Http\Controllers\Admin;

use App\Events\UserWasCreated;
use App\Http\Requests\UserRequest;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;


class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::get();
        return view('admin.user.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $user = new User;

        $this->authorize('create', $user);

        $roles = Role::with('permissions')->get();
        $permissions = Permission::pluck('name','id');
        $btnText = __("Crear Usuario");
        return view('admin.user.create', compact('user','btnText','roles','permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', new User);
       // Validar el formulario
        $data = $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
        ]);

        // Generar una contraseña
        $data['password'] = str_random(8);
        // Creamos el usuario
        $user = User::create($data);
        // Asignamos los roles
            // preguntamos si el usuario lleno los campos
            // en este caso los checkbox para que asigne los roles
        if($request->filled('roles'))
        {
            $user->assignRole($request->roles);
        }

        // Asignamos los permisos
            // preguntamos si el usuario lleno los campos
            // en este caso los checkbox para que asigne los permisos
        if ($request->filled('permissions'))
        {
            $user->givePermissionTo($request->permissions);
        }

        // Enviamos el email con datos de acceso
        UserWasCreated::dispatch($user, $data['password']);

        // Regresamos una respuesta añ  usuario
        return redirect()->route('users.index')->with('message', ['success', __("Usuario creado correctamente")]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $this->authorize('view', $user);
        return view('admin.user.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $this->authorize('update',$user);
        $roles = Role::with('permissions')->get();
        $permissions = Permission::pluck('name','id');
        $btnText = __("Editar Usuario");
        return view('admin.user.form', compact('user','btnText','roles','permissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $userRequest, User $user)
    {
        $this->authorize('update',$user);
        $user->update($userRequest->validated() );
        return back()->with('message', ['success', __("Usuario actualizado correctamente")]);


    }


    public function destroy(User $user)
    {
        $this->authorize('delete', $user);
        try {
            $user->delete();
            return back()->with('message', ['success', __("Curso eliminado correctamente")]);
        } catch (\Exception $exception) {
            return back()->with('message', ['danger', __("Error eliminando el curso")]);
        }

//        return redirect()->route('users.index')->with('message', ['success', __("Usuario Eliminado")]);

    }
}
