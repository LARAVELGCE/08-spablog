<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UsersPermissionsController extends Controller
{
    public function update(Request $request,User $user)
    {
        /*syncRoles : quita todos los roles del usuario y luego agrega
        todos los roles que le pasamos con este metodo */
        $user->syncPermissions($request->permissions);
        return back()->with('message', ['success', __("Los permisos han sido actualizados")]);
    }
}
