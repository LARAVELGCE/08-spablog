<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Requests\PostRequest;
use App\Post;
use App\Tag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;

class PostsController extends Controller
{
    public function index()
    {
        // mostrar los posts solo del usuario
       // $posts = auth()->user()->posts;

        $posts = Post::allowed()->get();
        return view('admin.post.index',compact('posts'));
    }

     public function store(PostRequest $postRequest)
    {
        $this->authorize('create',new Post);

        // solo se guardara el titutlo por los demas son null
        $post = Post::create([
            'title' => $postRequest->get('title'),
            'user_id' => auth()->id()
        ]);
        return redirect()->route('posts.edit', compact('post'));
    }


    public function edit(Post $post)
    {
        // le ponemos la autorizacion y le mandamos el nombre de la funcion
        // que esta en Post/Policy y como segundo parametro le pasamos el objeto que
        // queremos autorizar
        $this->authorize('update',$post);
        $btnText = __("Guardar Publicacion");
        $post = Post::with(['category','tags'])->whereSlug($post->slug)->first();
        return view('admin.post.form', compact('post','btnText'));
    }

    public function update(Post $post, PostRequest $postRequest )
    {
        $this->authorize('update',$post);
         /*  // return $postRequest->all();
            $post->title = $postRequest->get('title');
            $post->excerpt = $postRequest->get('excerpt');
            $post->body = $postRequest->get('body');
            $post->iframe = $postRequest->get('iframe');
            // en nuestra vista el select de category es  name="category_id" por eso lo obtengo el valor de category_id
            // si la categoria es encontrada solo retorna la categoria hallada ? $cat
            // pero si no lo encuentra  crea una nueva categoria y le asigna un id
            // siempre y cuando en la validacion debe comentar esto // Rule::exists('categories', 'id')
            $post->category_id = $postRequest->get('category_id');
            $post->published_at =  $postRequest->get('published_at');
            $post->save();*/

        // Finalmente esos tags se asignaran
        // en nuestra vista el select de tags es  name="tags[]" por eso lo obtengo todos los valores tags
            $post->update($postRequest->all());
            $post->syncTags($postRequest->get('tags'));
        // luego enviamos un mensaje de exito de envio
        return redirect()->route('posts.edit', compact('post'))->with('message', ['success', __('Post editado correctamente')]);
    }

    public function destroy(Post $post)
    {
        $this->authorize('delete',$post);
        $post->delete();

        return redirect()
            ->route('posts.index')
            ->with('message', ['success', __('La publicacion ha sido eliminada')]);
    }



}
