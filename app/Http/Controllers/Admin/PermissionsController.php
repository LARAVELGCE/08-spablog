<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Permission;

class PermissionsController extends Controller
{
    public function index()
    {
        $this->authorize('view', new Permission);

        return view('admin.permissions.index',[
            'permissions' => Permission::all()
        ]);
    }

    public function edit(Permission $permission)
    {
        $this->authorize('update', $permission);

        $btnText = __("Editar Permiso");
        return view('admin.permissions.form', [
            'permission' => $permission,

        ],compact('btnText'));
    }

    public function update(Request $request, Permission $permission)
    {
        $this->authorize('update', $permission);

        $data = $request->validate(['display_name' => 'required']);

        $permission->update($data);

        return back()->with('message', ['success', __("El permiso ha sido actualizado")]);
    }

}
