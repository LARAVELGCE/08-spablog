<?php

namespace App\Http\Controllers;

use App\Tag;
use Illuminate\Http\Request;

class TagsController extends Controller
{
    public function searchTag(Tag $tag)
    {
        $posts = $tag->posts()->published()->paginate(1);

        if (request()->wantsJson())
        {
            return $posts;
        }


        // Estamos enviado a pantala del welcome solo las post de dicha categoria
        return view('pages.home',
            [
                // Estamos enviado el tag
                'title' => "Publicacion del tag {$tag->name}" ,
                // Estamos enviado a pantala del welcome solo las post de dicha categoria
                'posts' => $posts

            ]);
    }
}
