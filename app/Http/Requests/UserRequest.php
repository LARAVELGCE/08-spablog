<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        // dd($this->route('user')->id) -> devuelve el id del usuario
        $rules = [
            'name' => 'required',
            'email' => [
                'required',
                Rule::unique('users')->ignore( $this->route('user')->id )
            ]
        ];

        // si el campo viene  lleno o escrito en el input del campo password
        if ( $this->filled('password') )
        {
            $rules['password'] = ['confirmed', 'min:6'];
        }

        return $rules;
    }


}
