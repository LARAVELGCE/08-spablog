<?php

namespace App\Http\Requests;
use Illuminate\Validation\Rule;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            // No hay validaciones ni para get ni para delete
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST': {
                return [
                    'title' => 'required|min:5',
                ];
            }
            // para actualizar

            case 'PUT': {
                return [
                    'title' => 'required|min:5',
                    'excerpt' => 'required|min:10',
                    'body' => 'required|min:30',
                    // para el category_id una regla es tambien que el mismo id debe existir en la tabla categories
                    'category_id' => [
                        'required',
                       // Rule::exists('categories', 'id')
                    ],
                    'tags' => [
                        'required',
//                        Rule::exists('tags', 'id')
                    ],
                    'published_at' => 'required|date',
                ];
            }

        }
    }
}
