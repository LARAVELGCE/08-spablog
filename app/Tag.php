<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    // Deshabilitamos la asiganacion masiva
    protected  $guarded = [];

    public function posts() {
        return $this->belongsToMany(Post::class);
    }

    // Slug para redirigit al categoria
    public function getRouteKeyName() {
        return 'slug';
    }

    public function setNameAttribute($name)
    {
        $this->attributes['name'] = $name;
        $this->attributes['slug'] = str_slug($name);
    }

}
