<?php

namespace App\Policies;

use App\User;
use App\Post;
use Illuminate\Auth\Access\HandlesAuthorization;

class PostPolicy
{
    use HandlesAuthorization;

    // para que los administradaores miren editen eliminien crea (cualquier accion que estan abajos )
    // solo para el usuario administrador
    // retorna
    // AQUI EL ADMIN TIENE ACCESO A TODAS LAS FUNCIONES CRUD
    public function before($user)
    {
        if ( $user->hasRole('Admin') )
        {
            return true;
        }
    }


    /**
     * Determine whether the user can view the post.
     *
     * @param  \App\User  $user
     * @param  \App\Post  $post
     * @return mixed
     */
    public function view(User $user, Post $post)
    {
        // comprobara si el id del usuario es propietario de dicho post
        // lista todos los post que son son dueños los id
        // y si le pasas otro te bota un error404
        return $user->id === $post->user_id || $user->hasPermissionTo('View posts');

    }

    /**
     * Determine whether the user can create posts.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        // tambien esta autorizado para crear post
        return $user->hasPermissionTo('Create posts');
    }

    /**
     * Determine whether the user can update the post.
     *
     * @param  \App\User  $user
     * @param  \App\Post  $post
     * @return mixed
     */
    public function update(User $user, Post $post)
    {
        // comprobara si el id del usuario es propietario de dicho post
        return $user->id === $post->user_id  || $user->hasPermissionTo('Update posts');
    }

    /**
     * Determine whether the user can delete the post.
     *
     * @param  \App\User  $user
     * @param  \App\Post  $post
     * @return mixed
     */
    public function delete(User $user, Post $post)
    {
        // comprobara si el id del usuario es propietario de dicho post
        return $user->id === $post->user_id || $user->hasPermissionTo('Delete posts');
    }
}
