require('./bootstrap');

//Importamos Vue para que Funcione
import Vue from 'vue';

// Js de las Rutas
import router from './routes'

// VueAnimate importarlo
//require('vue2-animate/dist/vue2-animate.min.css');

// Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('post-header', require('./components/PostHeader.vue'));
Vue.component('nav-bar', require('./components/NavBar.vue'));

Vue.component('posts-list', require('./components/PostsList'));
Vue.component('posts-list-item', require('./components/PostsListItem'));

Vue.component('category-link', require('./components/CategoryLink'));
Vue.component('tag-link', require('./components/TagLink'));
Vue.component('post-link', require('./components/PostLink'));

Vue.component('disqus-comments', require('./components/DisqusComments'));

Vue.component('paginator', require('./components/Paginator'));
Vue.component('pagination-links', require('./components/PaginationLinks'));

Vue.component('social-links', require('./components/SocialLinks'));
// registrar el componente del Forumulario de contacto
Vue.component('contact-form', require('./components/ContactForm'));


const app = new Vue({
    el: '#app',
    // aqui le pasamos a Vue como instancia para que funcione
    // router: router  => el router1 => Vue.use(Router); y router2 => let router = new Router({}) es igual al nombre de la variable
    // para simplificar y como los dos se llaman lo mismo por eso coloque una sola vez router
    router
});
