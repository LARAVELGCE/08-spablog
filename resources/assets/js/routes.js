
import Vue from 'vue';
// importamos vue-router
import Router from 'vue-router';



//vamos a decirle que se use
Vue.use(Router);

// rutas
// definimos la variable router que es igual a nueva  instancia de Router
// para poder exportarlo tenemos que cambiar el nombre que teniamos antes a export default new nombredelaexportacion
// en mi caso Router la cual sera llamada desde app.js
export default  new Router({
    // routes: Maneja de rutas que se maneja a traves del objeto
    routes: [
        {
            // cada ruta necesita un url y un componente
            path: '/',
            // y dentro de esa ruta se va a mostrar este componente cuando este en la vista principal /

            //nombre de la ruta
            name: 'home',
            component: require('./views/Home')
        },
        {
            path: '/nosotros',
            name: 'about',
            component: require('./views/About')
        },
        {
            path: '/archivo',
            name: 'archive',
            component: require('./views/Archive')
        },
        {
            path: '/contacto',
            name: 'contact',
            component: require('./views/Contact')
        },
        // Ruta individual de cada posts como ves le voy a enviar un parametro :url
        {
            path: '/blog/:url',
            name: 'posts_show',
            component: require('./views/PostsShow'),
            props : true
        },
        // Ruta para filtrar por cada categoria
        {
            path: '/categorias/:category',
            name: 'category_posts',
            component: require('./views/CategoryPosts'),
            props : true
        },
        // Ruta para filtrar por cada tags paso 1 y le declaro la variable tag
        {
            path: '/tags/:tag',
            name: 'tags_posts',
            component: require('./views/TagsPosts'),
            // para que me pueda aceptar la propiedad
            // y al momento que props sea true todos los parametros de  path: '/tags/:tag',
            // seran pasados a la vista TagsPosts.vue
            props : true
        },


        {
            path: '*',
            component: require('./views/404')
        }

    ],
    // esta linea de codigo agregara al css al menu activo de tu navegacion ya que tu anterormente tenias una clase
    // active para que se mantenga transparente cuando estaba en dicho url ya reemplazarias la clase router-link-exact-active
    // a la clase active como esta declarada aqui estas se usan para barra  de navegacion de primer nivel
    linkExactActiveClass: 'active',
    // para quitar el hash (#) de las url de VueJS
    mode :'history',
    // para segundo nivel o submenu le colocas estas prpopiedad valor para reemplazar la clase que viene `pr defecto en VueJS
    //  linkActiveClass: 'router-active'
    scrollBehavior(){
    return {x:0, y:0}
}

});