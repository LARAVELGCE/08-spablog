@extends('layout')

@section('content')
    <section class="posts container">
        {{--si esta seteada --}}
        @if (isset($title))
            <h3>{{ $title }}</h3>
        @endif
        @forelse($posts as $post)
            <article class="post">
                {{--Le pasamos un metodo con el parametro home
                para indicarle que estamos en la vista home--}}
                @include($post->viewType('home'))
                <div class="content-post">

                    @include('posts.header')

                    <h1>{{ $post->title }}</h1>
                    <div class="divider"></div>
                    <p>{{ $post->excerpt }}</p>
                    <footer class="container-flex space-between">
                        <div class="read-more">
                            <a href="{{ route('posts.detail', $post->slug) }}" class="text-uppercase c-green">Leer Más</a>
                        </div>
                        @include('posts.tags')
                    </footer>
                </div>
            </article>
        @empty   {{--le dices que no hay cursos --}}
        <article class="post">
            <div class="content-post">
            {{ __("No hay ningún posts disponible") }}
        </div>
        </article>
        @endforelse
    </section><!-- fin del div.posts.container -->

    {{--para que se mantenga siempre ?month=9&year=2013 y de ahi se muestre ?page2--}}
    {{ $posts->appends(request()->all())->links() }}

    {{--   <div class="pagination">
           <ul class="list-unstyled container-flex space-center">
               <li><a href="#" class="pagination-active">1</a></li>
               <li><a href="#">2</a></li>
               <li><a href="#">3</a></li>
           </ul>
       </div>--}}

@endsection
