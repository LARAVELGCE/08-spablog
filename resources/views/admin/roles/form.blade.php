@extends('admin.layout')
@section('header')

@endsection
@section('content')
    <div class="row">

        <form
                method="POST"
                {{--Si no exite el id del curso se va procesar store (guardar una nuevo )
                si lo tiene solo va actualizar el id del regustro y ademas le paso el slug --}}
                action="{{ ! $role->id ? route('roles.store') : route('roles.update', ['id' => $role->id])}}"
                {{--que no valide el navegador --}}
                novalidate

        >

            @if($role->id)
                @method('PUT')
            @endif

            @csrf


            <div class="col-md-6">

                <div class="box box-warning">
                    <div class="box-header">
                        <h3 class="box-title">Roles</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        @if ($role->exists)
                            <div class="form-group has-feedback">
                                <label for="name">
                                    {{ __("Identificador") }}
                                </label>
                                <input
                                        name="name"
                                        id="name"
                                        class="form-control"
                                        value="{{ $role->name }}"
                                        disabled
                                />
                            </div>
                        @else
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} has-feedback">
                                <label for="name">
                                    {{ __("Identificador") }}
                                </label>
                                <input
                                        name="name"
                                        id="name"
                                        class="form-control"
                                        value="{{ old('name') ?: $role->name }}"

                                />

                                @if ($errors->has('name'))
                                    <span class="help-block" role="alert">
                                                {{ $errors->first('name') }}
                                </span>
                                @endif
                            </div>
                        @endif

                            <div class="form-group{{ $errors->has('display_name') ? ' has-error' : '' }} has-feedback">
                                <label for="display_name">
                                    {{ __("Nombre") }}
                                </label>
                                <input
                                        name="display_name"
                                        id="display_name"
                                        class="form-control"
                                        value="{{ old('display_name') ?: $role->display_name }}"

                                />

                                @if ($errors->has('display_name'))
                                    <span class="help-block" role="alert">
                                                {{ $errors->first('display_name') }}
                                </span>
                                @endif
                            </div>

                        <div class="form-group col-md-6">
                            <label>Permisos</label>
                            @include('admin.permissions.checkboxes', ['model' => $role])
                        </div>


                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block">
                                {{ __($btnText) }}
                            </button>
                        </div>
                    </div>
                </div>
            </div>

        </form>


    </div>

@endsection
