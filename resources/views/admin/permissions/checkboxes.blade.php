@forelse ($permissions as $id => $name)
    <div class="checkbox">
        <label>
            {{--aqui hubo un problemas al momemtno de interactuar con la  variable $user y por eso le colocamos
            $model ya que lo vamos a llamar asi desde el archivo que lo requiera
             @include('admin.permissions.checkboxes', ['model' => $role])
             --}}
            <input name="permissions[]" type="checkbox" value="{{ $name }}"
                   {{--verificamos si el  rol tiene los mismo permisos que los que estan la tabla permiso a traves de su id
                   y tambien si realmente los permisos que tiene seleccionado actuamente
                   coincide con los nombre de los permisos(true|false)
                   --}}
                    {{
                     $model->permissions->contains($id) || collect( old('permissions') )->contains($name)
                     ? 'checked' : ''
                    }}>
            {{ $name }}
        </label>
    </div>

@empty

@endforelse