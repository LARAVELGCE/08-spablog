@extends('admin.layout')
@section('header')
    <h1>
        Todas las publicaciones
        <small>Optional description</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Inicio</a></li>
        <li class="active">Posts</li>
    </ol>
@endsection
@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Listado de Publicaciones</h3>
            <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Crear Publicacion</button>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="posts-table" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Titulo</th>
                    <th>Extracto</th>
                    <th>Acciones</th>
                </tr>
                </thead>
                <tbody>
                @forelse($posts as $post)
                <tr>
                    <td>{{ $post->id }}</td>
                    <td>{{ $post->title }}</td>
                    <td>{{ $post->excerpt }}</td>
                    <td>
                        <a href="{{ route('posts.detail',$post) }}" target="_blank" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>
                        <a href="{{ route('posts.edit',$post) }}" class="btn btn-xs btn-info"><i class="fa fa-pencil"></i></a>
                        <form method="POST"
                              action="{{ route('posts.destroy', $post) }}"
                              style="display: inline">
                            {{ csrf_field() }} {{ method_field('DELETE') }}
                            <button class="btn btn-xs btn-danger"
                                    onclick="return confirm('¿Estás seguro de querer eliminar esta publicación?')"
                            ><i class="fa fa-times"></i></button>
                        </form>

                    </td>
                </tr>
                @empty   {{--le dices que no hay cursos --}}
                <div class="alert alert-dark">
                    {{ __("No hay ningún curso disponible") }}
                </div>
                @endforelse

                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
@endsection
@section('scripts')

@endsection
@push('styles')
    <link href="/css/datatables.css" rel="stylesheet">
@endpush
@push('scripts')
    <script src="/js/datatables.js" ></script>
    <script>
        $(function () {
            $('#posts-table').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false
            });
        });
    </script>
@endpush