@extends('admin.layout')
@section('header')

@endsection
@section('content')
    <div class="row">

        <form
                method="POST"
                {{--Si no exite el id del curso se va procesar store (guardar una nuevo )
                si lo tiene solo va actualizar el id del regustro y ademas le paso el slug --}}
                action="{{route('users.store', ['id' => $user->id])}}"
                {{--que no valide el navegador --}}
                novalidate
                {{--subida de archivos --}}
                enctype="multipart/form-data"
        >

            @csrf


            <div class="col-md-6">

                <div class="box box-warning">
                    <div class="box-header">
                        <h3 class="box-title">Datos Personales</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} has-feedback">
                            <label for="name">
                                {{ __("Nombre") }}
                            </label>
                            <input
                                    name="name"
                                    id="name"
                                    class="form-control"
                                    value="{{ old('name') }}"

                            />

                            @if ($errors->has('name'))
                                <span class="help-block" role="alert">
                                                {{ $errors->first('name') }}
                                </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} has-feedback">
                            <label for="email">
                                {{ __("Email") }}
                            </label>
                            <input
                                    type="email"
                                    name="email"
                                    id="email"
                                    class="form-control"
                                    value="{{ old('email') }}"

                            />

                            @if ($errors->has('email'))
                                <span class="help-block" role="alert">
                                                {{ $errors->first('email') }}
                                </span>
                            @endif
                        </div>
                        <div class="form-group col-md-6">
                            <label for="">Roles</label>
                            @include('admin.roles.checkboxes')
                        </div>
                        <div class="form-group col-md-6">
                            <label for="">Roles</label>
                            @include('admin.permissions.checkboxes', ['model' => $user])
                        </div>

                        <span class="help-block">La contraseña sera generada y enviada al usuario via Email</span>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block">
                                {{ __($btnText) }}
                            </button>
                        </div>
                    </div>
                </div>
            </div>

        </form>

    </div>

@endsection
