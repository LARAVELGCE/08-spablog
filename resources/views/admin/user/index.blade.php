@extends('admin.layout')
@section('header')
    <h1>
        USUARIOS
        <small>Listado</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Inicio</a></li>
        <li class="active">Usuarios</li>
        @can('create', $users->first())
            <a href="{{ route('users.create') }}" class="btn btn-primary pull-right">
                <i class="fa fa-plus"></i>  Crear usuario
            </a>
        @endcan

    </ol>
@endsection
@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Listado de Usuario</h3>
            <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Crear Publicacion</button>

        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="users-table" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Email</th>
                    <th>Roles</th>
                    <th>Acciones</th>
                </tr>
                </thead>
                <tbody>
                @forelse($users as $user)
                    @can('view', $user)
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->getRoleNames()->implode(', ') }}</td>
                        <td>

                            @can('view', $user)
                            <a href="{{ route('users.show',$user) }}"  class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>
                            @endcan

                             @can('update', $user)
                            <a href="{{ route('users.edit',$user) }}" class="btn btn-xs btn-info"><i class="fa fa-pencil"></i></a>
                            @endcan

                            @can('delete', $user)
                            <form method="POST"
                                  action="{{ route('users.destroy', $user) }}"
                                  style="display: inline">
                                {{ csrf_field() }} {{ method_field('DELETE') }}
                                <button class="btn btn-xs btn-danger"
                                        onclick="return confirm('¿Estás seguro de querer eliminar a este usuario?')"
                                ><i class="fa fa-times"></i></button>
                            </form>
                            @endcan

                        </td>
                    </tr>
                    @endcan
                @empty   {{--le dices que no hay cursos --}}
                <div class="alert alert-dark">
                    {{ __("No hay ningún curso disponible") }}
                </div>

                @endforelse

                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
@endsection
@section('scripts')

@endsection
@push('styles')
    <link href="/css/datatables.css" rel="stylesheet">
@endpush
@push('scripts')
    <script src="/js/datatables.js" ></script>
    <script>
        $(function () {
            $('#users-table').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false
            });
        });
    </script>
@endpush