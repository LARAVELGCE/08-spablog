<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <form
            method="POST"
            {{--Si no exite el id del curso se va procesar store (guardar una nuevo )
            si lo tiene solo va actualizar el id del regustro y ademas le paso el slug --}}
            action="{{route('posts.store','#create')}}"
            {{--que no valide el navegador --}}
            novalidate
            {{--subida de archivos --}}
            enctype="multipart/form-data"
    >

        @csrf

        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Agregar Titulo al Post</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }} has-feedback">
                        <label for="name">
                            {{ __("Titulo de la publicacion") }}
                        </label>
                        <input
                                name="title"
                                id="post-title"
                                class="form-control"
                                value="{{ old('title')}}"
                                placeholder="Ingrese aqui el titulo de la publicacion"
                                autofocus
                        />

                        @if ($errors->has('title'))
                            <span class="help-block" role="alert">
                                                {{ $errors->first('title') }}
                                            </span>
                        @endif
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary">Guardar Publicación</button>
                </div>
            </div>
        </div>
    </form>
</div>

@push('scripts')
    <script>
        if( window.location.hash === '#create')
        {
            $('#myModal').modal('show');
        }
        $('#myModal').on('hide.bs.modal',function () {
            window.location.hash = '#'
            //console.log("Hola Mud")
        });
        $('#myModal').on('shown.bs.modal',function () {
            $('#post-title').focus();
            window.location.hash = '#create'
            //console.log("Hola Mud")
        });
    </script>
@endpush