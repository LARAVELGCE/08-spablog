@extends('layout')

@section('facebook')
    <meta property="og:url" content="https://yarnpkg.com/en/docs/usage/" />
    <meta property="og:site_name" content="Yarn"/>
    <meta property="og:title" content="Yarn" />
    <meta property="og:image" content="https://yarnpkg.com/assets/og_image.png" />
    <meta property="og:description" content="Fast, reliable, and secure dependency management." />
@endsection
@section('meta-tile', $post->title)
@section('meta-description', $post->excerpt)
@section('content')
    <article class="post image-w-text container">
        {{--aqui no le pasamos nada--}}
        @include($post->viewType())
        <div class="content-post">
            @include('posts.header')
            <h1>{{ $post->title }}</h1>
            <div class="divider"></div>
            <div class="image-w-text">
                {!!  $post->body !!}
            </div>

            <footer class="container-flex space-between">
                @include('partials.social-links',['description' => $post->title])
                @include('posts.tags')
            </footer>
            <div class="comments">
                <div class="divider"></div>
                <div id="disqus_thread"></div>
                @include('partials.disqus')
            </div><!-- .comments -->
        </div>
    </article>
@stop
@push('styles')
    <link href="/css/bootstrap_carousel.css" rel="stylesheet">
@endpush
@push('scripts')
    <script id="dsq-count-scr" src="//zendero.disqus.com/count.js" async></script>
    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <script  src="/js/bootstrap_carousel.js" async></script>
@endpush
