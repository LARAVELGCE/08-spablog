<div class="gallery-photos masonry">
    @foreach ($post->photos->take(4) as $photo)
        <figure class="grid-item grid-item--height2">
            @if ($loop->iteration === 4)
                <div class="overlay">{{ $post->photos_count }} Fotos</div>
            @endif
            <img src="{{ $photo->pathAttachment() }}" alt="" class="img-responsive">
        </figure>
    @endforeach
</div>