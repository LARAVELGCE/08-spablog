<div class="tags container-flex">
    @forelse($post->tags as $tag)
        <span class="tag c-gris text-capitalize"><a href="{{ route('search.tag', $tag) }}">#{{ $tag->name}}</a></span>
    @empty   {{--le dices que no hay cursos --}}
    <div class="alert alert-dark">
        <span class="tag c-gris text-capitalize"> {{ __("No tiene  etiquetas") }}</span>
    </div>
    @endforelse
</div>