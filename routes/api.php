<?php
// Solo estamos reutilizando el controlador de routes/web.php donde se manejan las rutas de LARAVEL
// por defecto todos estas rutas es llamodo con un prefijo /api por ejemplo este ruta get su ruta sera
// =>>>  /api/posts
Route::get('posts', 'PagesController@index');

// Ruta de cada post
Route::get('blog/{post}', 'PostsController@show');

// fILTRO por categoria

Route::get('categorias/{category}', 'CategoriesController@searchCategory');

// Filtro por etiquetas

Route::get('tags/{tag}', 'TagsController@searchTag');

// Mirar todos datos de la pagina archivo

Route::get('archivo', 'PagesController@archive');

// Para procesar la informacion del formulario
Route::post('messages', function(){

    // Validar datos
    // Enviar un email

    return response()->json([
        'status' => 'OK'
    ]);
});
