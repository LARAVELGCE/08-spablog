<?php

use Illuminate\Database\Seeder;
use App\User;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class UserRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::truncate();
        User::truncate();
        Permission::truncate();

        // Llenar la tabla Role
        $adminRole = Role::create(['name' => 'Admin', 'display_name' => 'Administrador']);
        $writerRole = Role::create(['name' => 'Writer', 'display_name' => 'Escritor']);
    /*
     * 1	Admin	web	    2018-08-07 23:30:09	  2018-08-07 23:30:09
       2	Writer	web	    2018-08-07 23:30:09	  2018-08-07 23:30:09
    */



        $viewPostsPermission = Permission::create([
            'name' => 'View posts',
            'display_name' => 'Ver publicaciones'
        ]);
        $createPostsPermission = Permission::create([
            'name' => 'Create posts',
            'display_name' => 'Crear publicaciones'
        ]);
        $updatePostsPermission = Permission::create([
            'name' => 'Update posts',
            'display_name' => 'Actualizar publicaciones'
        ]);
        $deletePostsPermission = Permission::create([
            'name' => 'Delete posts',
            'display_name' => 'Eliminar publicaciones'
        ]);

        $viewUsersPermission = Permission::create([
            'name' => 'View users',
            'display_name' => 'Ver usuarios'
        ]);
        $createUsersPermission = Permission::create([
            'name' => 'Create users',
            'display_name' => 'Crear usuarios'
        ]);
        $updateUsersPermission = Permission::create([
            'name' => 'Update users',
            'display_name' => 'Actualizar usuarios'
        ]);
        $deleteUsersPermission = Permission::create([
            'name' => 'Delete users',
            'display_name' => 'Eliminar usuarios'
        ]);

        $viewRolesPermission = Permission::create([
            'name' => 'View roles',
            'display_name' => 'Ver roles'
        ]);
        $createRolesPermission = Permission::create([
            'name' => 'Create roles',
            'display_name' => 'Crear roles'
        ]);
        $updateRolesPermission = Permission::create([
            'name' => 'Update roles',
            'display_name' => 'Actualizar roles'
        ]);
        $deleteRolesPermission = Permission::create([
            'name' => 'Delete roles',
            'display_name' => 'Eliminar roles'
        ]);

        $viewPermissionsPermission = Permission::create([
            'name' => 'View permissions',
            'display_name' => 'Ver permisos'
        ]);
        $updatePermissionsPermission = Permission::create([
            'name' => 'Update permissions',
            'display_name' => 'Actualizar permisos'
        ]);







        $admin= new User();
        $admin->name = 'Geancarlos CE';
        $admin->email = 'gean@ed.team';
        $admin->password = 'secret';
        $admin->save();
        $admin->assignRole($adminRole);

        $writer = new User();
        $writer->name = 'VBQ';
        $writer->email = 'todovb@ed.team';
        $writer->password = 'secret';
        $writer->save();
        // Llenar la tabla User
         /*
        1	Geancarlos CE	gean@ed.team	 $2y$10$juy9CrEsp0XYMuw9LheDVeLoNqlRepZYAOclZHKDHcHx.kShfxDJe	LrF5fM2E7U6Q7wvcCU8msPYpsYDT1p0m9P7yWcqQk982Ift4mBRQN0xjim6W	2018-08-07 23:30:09	2018-08-07 23:30:09
        2	VBQ	            todovb@ed.team	 $2y$10$ZeM5maNfPR6qQZV65tchrePPjgPLZIBXcZMfeI4JKjhEUMTkdKNM.		2018-08-07 23:30:10	2018-08-07 23:30:10
        */
        $writer->assignRole($writerRole);

        // Llenar la tabla model_has_roles
        /*
            1	App\User	1
            2	App\User	2
        */




    }
}
