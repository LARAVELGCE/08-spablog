<?php

use Faker\Generator as Faker;

$factory->define(App\Post::class, function (Faker $faker) {
    $title = $faker->unique()->jobTitle;
    return [
        'title' =>  $title,
        'excerpt' => $faker->text,
        'body' =>  $faker->paragraph(15, false),
        'slug' => str_slug($title, '-'),
        'category_id' => \App\Category::all()->random()->id,
        'user_id' => \App\User::all()->random()->id,
        'published_at' =>  $faker->date()
    ];
});
