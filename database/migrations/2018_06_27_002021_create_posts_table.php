<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('slug')->unique()->nullable();;
            $table->mediumText('excerpt')->nullable();
            $table->mediumText('iframe')->nullable();
            $table->text('body')->nullable();

            // Relacion con Categoria 1 a muchos
            $table->unsignedInteger('category_id')->nullable();
            $table->foreign('category_id')->references('id')->on('categories');

            // Relacion con usuario 1 a muchos
            $table->unsignedInteger('user_id');
            // Eliminando a sus post asociados a este
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');;

            $table->timestamp('published_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
